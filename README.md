Yilmar Hernandez

1. Reto1
Creamos la imagen para contenerizar la aplicación node utilizando el Dockerfile ubicado en la ruta nodejs y ejecutando el siguiente comando ubicandonos en el directorio **reto1/nodejs**

docker build -t backend_node:v1 .

Importante mantener el nombre de la etiqueta, la que será utlizada mas adelante.

Creamos el contenedor publicando el puerto 80 (localhost) al 3000 (contenedor) con el fin de testear la aplicación
docker run -itd -p 80:3000 backend_node:v1

Ahora probamos
curl http://localhost
curl http://localhost/public
curl http://localhost/private

2. Reto2

Se crea el directorio reto2, dentro de el encontramos dos directorios mas con el siguiente contenido:
2.1 nginx
Se encuentra el contenido del contenedor nginx que actuará como frontend, el archivo Dockerfile y el directorio app necesario para su correcta creación.

Compilamos la imagen, ubicandonos dentro del directorio nginx
docker build -t frontend_nginx:v1 .

2.2 compose
Contiene el archivo docker-compose para subir el ambiente (frontend y backend)
ejecutamos el archivo docker-compose up -d

Luego, añadimos a nuestro /etc/hosts la entrada 
IP donde se ejecuta el contenedor       frontend.retodevops.com

Luego accedemos a traves de curl o a traves de un navegador.

Las credenciales de acceso a /private
Username: retodevops
Password: RETOdevops

3. Reto4
NOTA: Para este reto se subieron las imagenes previamente creadas a un repositorio publico yilmar1734/retodevops
Se creó un directorio llamada reto4 con el siguiente contenido:

Si el HPA no funciona correctamente o si tiene errores al traer las metricas debemos editar el pod ubicados en el namespace kube-system y adicionar la siguiente linea debajo del bloque commands.
kubectl edit deployment metrics-server -n kube-system -> Comando para elditar
- --kubelet-insecure-tls -> Liena a adicionar

